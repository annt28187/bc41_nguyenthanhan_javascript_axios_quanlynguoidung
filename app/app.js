const URL_API = 'https://63bea7fce348cb0762149aa0.mockapi.io';
var ndArr = [];
var account = '';

function fetchUsersList() {
  enableLoading();
  axios({
    url: `${URL_API}/users`,
    method: 'GET',
  })
    .then(function (res) {
      disableLoading();
      ndArr = [...res.data];
      renderUsersList(res.data);
    })
    .catch(function (err) {
      disableLoading();
      console.log(err);
    });
}
fetchUsersList();

function xoaNguoiDung(id) {
  enableLoading();
  axios({
    url: `${URL_API}/users/${id}`,
    method: 'DELETE',
  })
    .then(function (res) {
      location.reload();
      disableLoading();
      console.log(res);
    })
    .catch(function (err) {
      disableLoading();
      console.log(err);
    });
}

function themNguoiDung() {
  var user = layThongTinTuForm();
  var isValid = validationAdd(user);
  if (isValid) {
    enableLoading();
    axios({
      url: `${URL_API}/users`,
      method: 'POST',
      data: layThongTinTuForm(),
    })
      .then(function (res) {
        disableLoading();
        console.log(res);
        $('#myModal').modal('hide');
        fetchUsersList();
      })
      .catch(function (err) {
        disableLoading();
        console.log(err);
      });
  } else {
    disableLoading();
  }
}

function suaNguoiDung(id) {
  enableLoading();
  $('#myModal').modal('show');
  getID('btnCapNhatND').style.display = 'block';
  getID('btnThemND').style.display = 'none';
  axios({
    url: `${URL_API}/users/${id}`,
    method: 'GET',
  })
    .then(function (res) {
      disableLoading();
      console.log(res);
      hienThiThongTinLenForm(res.data);
      document.querySelector('#idND span').innerHTML = res.data.id;
      account = res.data.taiKhoan;
      console.log('🚀 ~ account', account);
    })
    .catch(function (err) {
      disableLoading();
      console.log(err);
    });
}

function capNhatNguoiDung() {
  enableLoading();
  var user = layThongTinTuForm();
  var idND = document.querySelector('#idND span').innerHTML * 1;
  var isValid = validationUpdate(user, account);
  if (isValid) {
    axios({
      url: `${URL_API}/users/${idND}`,
      method: 'PUT',
      data: user,
    })
      .then(function (res) {
        disableLoading();
        console.log(res);
        $('#myModal').modal('hide');
        fetchUsersList();
        getID('btnThemND').style.display = 'block';
      })
      .catch(function (err) {
        disableLoading();
        console.log(err);
      });
  }
}

getID('btnThemNguoiDung').addEventListener('click', function () {
  getID('formND').reset();
  getID('idND').style.display = 'none';
  getID('btnCapNhatND').style.display = 'none';
});
