const NguoiDung = function (
  _taiKhoan,
  _hoTen,
  _matKhau,
  _email,
  _hinhAnh,
  _loaiND,
  _ngonNgu,
  _moTa
) {
  this.taiKhoan = _taiKhoan;
  this.hoTen = _hoTen;
  this.matKhau = _matKhau;
  this.email = _email;
  this.ngonNgu = _ngonNgu;
  this.moTa = _moTa;
  this.hinhAnh = _hinhAnh;
  this.loaiND = _loaiND;
};
