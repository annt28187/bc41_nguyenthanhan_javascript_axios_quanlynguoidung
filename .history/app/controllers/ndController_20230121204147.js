function getID(x) {
  return document.getElementById(x);
}

function showMessageError(idError, message) {
  getID(idError).innerText = message;
}

function enableLoading() {
  getID('loading').style.display = 'flex';
}

function disableLoading() {
  getID('loading').style.display = 'none';
}

function renderUsersList(userArr) {
  var contentHTML = '';
  userArr.forEach(function (user) {
    var contentTR = `<tr>
                        <td>${user.id}</td>
                        <td>${user.taiKhoan}</td>
                        <td>${user.matKhau}</td>
                        <td>${user.hoTen}</td>
                        <td>${user.email}</td>
                        <td>${
                          // user.ngonNgu
                          user.ngonNgu == 'ITALIAN'
                            ? "<span class='text-primary'>ITALIAN</span>"
                            : user.ngonNgu == 'FRENCH'
                            ? "<span class='text-success'>FRENCH</span>"
                            : user.ngonNgu == 'JAPANESE'
                            ? "<span class='text-info'>JAPANESE</span>"
                            : user.ngonNgu == 'CHINESE'
                            ? "<span class='text-danger'>CHINESE</span>"
                            : user.ngonNgu == 'RUSSIAN'
                            ? "<span class='text-warning'>RUSSIAN</span>"
                            : user.ngonNgu == 'SWEDEN'
                            ? "<span class='text-muted'>SWEDEN</span>"
                            : "<span class='text-body'>SPANISH</span>"
                        }</td>
                        <td>${
                          user.loaiND
                            ? "<span class='text-primary'>Giáo viên</span>"
                            : "<span class='text-success'>Học viên</span>"
                        }</td>
                        <td> <button onclick="xoaNguoiDung(${
                          user.id
                        })" class="btn btn-danger">Xoá</button> 
                        <button onclick="suaNguoiDung(${
                          user.id
                        })"class="btn btn-warning" data-toggle="modal" data-target="#myModal">Sửa</button>
                      </td>
                    </tr>`;
    contentHTML += contentTR;
  });
  document.getElementById('tblDanhSachNguoiDung').innerHTML = contentHTML;
}

function layThongTinTuForm() {
  var _taiKhoan = document.getElementById('taiKhoan').value;
  var _hoTen = document.getElementById('hoTen').value;
  var _matKhau = document.getElementById('matKhau').value;
  var _email = document.getElementById('email').value;
  var _hinhAnh = document.getElementById('hinhAnh').value;
  var _loaiNguoiDung = document.getElementById('loaiNguoiDung').value;
  var _loaiNgonNgu = document.getElementById('loaiNgonNgu').value;
  var _moTa = document.getElementById('moTa').value;
  var user = new NguoiDung {
    _taiKhoan,
    _hoTen,
    _matKhau,
    _email,
    _hinhAnh,
    _loaiNguoiDung,
    _loaiNgonNgu,
    _moTa,
  };
  return user;
}

function hienThiThongTinLenForm(user) {
  document.getElementById('taiKhoan').value = user.taiKhoan;
  document.getElementById('hoTen').value = user.hoTen;
  document.getElementById('matKhau').value = user.matKhau;
  document.getElementById('email').value = user.email;
  document.getElementById('hinhAnh').value = user.hinhAnh;
  document.getElementById('loaiNguoiDung').value = user.loaiND;
  document.getElementById('loaiNgonNgu').value = user.ngonNgu;
  document.getElementById('moTa').value = user.moTa;
}

function timKiemViTri(userND, ndArr) {
  var viTri = -1;
  for (var i = 0; i < ndArr.length; i++) {
    var nd = ndArr[i];
    if (nd.taiKhoan == userND) {
      viTri = i;
      break;
    }
  }
  return viTri;
}
