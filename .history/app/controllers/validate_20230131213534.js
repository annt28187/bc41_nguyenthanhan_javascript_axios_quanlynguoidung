function kiemTraTrung(userND, ndArr) {
  var viTri = ndArr.findIndex(function (user) {
    return user.taiKhoan == userND;
  });
  if (viTri != -1) {
    showMessageError('tbTaiKhoan', 'Tài khoản ND đã tồn tại');
    getID('tbTaiKhoan').style.display = 'block';
    return false;
  } else {
    showMessageError('tbTaiKhoan', '');
    getID('tbTaiKhoan').style.display = 'none';
    return true;
  }
}

function kiemTraRong(value, idError, message) {
  if (value == '') {
    showMessageError(idError, message);
    getID(idError).style.display = 'block';
    return false;
  } else {
    showMessageError(idError, '');
    getID(idError).style.display = 'none';
    return true;
  }
}

function kiemTraChu(value, idError, message) {
  // var regLetter = /^[A-Za-z]+$/;
  var regLetter =
    /^[a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s\W|_]+$/;
  var isLetter = regLetter.test(value);
  if (isLetter) {
    showMessageError(idError, '');
    getID(idError).style.display = 'none';
    return true;
  } else {
    showMessageError(idError, message);
    getID(idError).style.display = 'block';
    return false;
  }
}

function kiemTraSo(value, idError, message) {
  var regNumber = /^\d+$/;
  var isNumber = regNumber.test(value);
  if (isNumber) {
    showMessageError(idError, '');
    getID(idError).style.display = 'none';
    return true;
  } else {
    showMessageError(idError, message);
    getID(idError).style.display = 'block';
    return false;
  }
}

function kiemTraEmail(value) {
  var regEmail =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  var isEmail = regEmail.test(value);
  if (isEmail) {
    showMessageError('tbEmail', '');
    getID('tbEmail').style.display = 'none';
    return true;
  } else {
    showMessageError('tbEmail', 'Email không hợp lệ');
    getID('tbEmail').style.display = 'block';
    return false;
  }
}

function kiemTraLuaChon(value, idError, message) {
  var tagSelect = getID(value);
  if (tagSelect.selectedIndex == 0) {
    showMessageError(idError, message);
    getID(idError).style.display = 'block';
    return false;
  } else {
    showMessageError(idError, '');
    getID(idError).style.display = 'none';
    return true;
  }
}

function kiemTraMatKhau(value) {
  var regPass = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{6,8}$/;
  if (value.match(regPass)) {
    showMessageError('tbMatKhau', '');
    getID('tbMatKhau').style.display = 'none';
    return true;
  } else {
    showMessageError(
      'tbMatKhau',
      'Mật khẩu từ 6-8 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt)'
    );
    getID('tbMatKhau').style.display = 'block';
    return false;
  }
}

function kiemTraMoTa(value, maxLength, idError) {
  var length = value.length;
  if (length > maxLength) {
    getID(idError).innerText = `Không vượt quá ${maxLength} ký tự`;
    getID(idError).style.display = 'block';
    return false;
  } else {
    getID(idError).innerText = '';
    getID(idError).style.display = 'none';
    return true;
  }
}
