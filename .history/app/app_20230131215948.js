const URL_API = 'https://63bea7fce348cb0762149aa0.mockapi.io';

function fetchUsersList() {
  enableLoading();
  axios({
    url: `${URL_API}/users`,
    method: 'GET',
  })
    .then(function (res) {
      disableLoading();
      renderUsersList(res.data);
    })
    .catch(function (err) {
      disableLoading();
      console.log(err);
    });
}
fetchUsersList();

function xoaNguoiDung(id) {
  enableLoading();
  axios({
    url: `${URL_API}/users/${id}`,
    method: 'DELETE',
  })
    .then(function (res) {
      location.reload();
      disableLoading();
      console.log(res);
    })
    .catch(function (err) {
      disableLoading();
      console.log(err);
    });
}

function themNguoiDung() {
  var isValid = validation();
  if (isValid) {
    var user = layThongTinTuForm();
    enableLoading();
    axios({
      url: `${URL_API}/users`,
      method: 'POST',
      data: user,
    })
      .then(function (res) {
        disableLoading();
        console.log(res);
        fetchUsersList();
      })
      .catch(function (err) {
        disableLoading();
        console.log(err);
      });
  }
}

function suaNguoiDung(id) {
  enableLoading();
  getID('btnCapNhatND').style.display = 'block';
  getID('btnThemND').style.display = 'none';
  axios({
    url: `${URL_API}/users/${id}`,
    method: 'GET',
  })
    .then(function (res) {
      disableLoading();
      console.log(res);
      hienThiThongTinLenForm(res.data);
      document.querySelector('#idND span').innerHTML = res.data.id;
    })
    .catch(function (err) {
      disableLoading();
      console.log(err);
    });
}

function capNhatNguoiDung() {
  var user = layThongTinTuForm();
  var idND = document.querySelector('#idND span').innerHTML * 1;
  enableLoading();
  axios({
    url: `${URL_API}/users/${idND}`,
    method: 'PUT',
    data: user,
  })
    .then(function (res) {
      disableLoading();
      console.log(res);
      fetchUsersList();
      getID('btnThemND').style.display = 'block';
    })
    .catch(function (err) {
      disableLoading();
      console.log(err);
    });
}

getID('btnThemNguoiDung').addEventListener('click', function () {
  getID('formND').reset();
  getID('idND').style.display = 'none';
  getID('btnCapNhatND').style.display = 'none';
});
