const URL_API = 'https://63bea7fce348cb0762149aa0.mockapi.io';
var dsnd = [];
function fetchUsersList() {
  enableLoading();
  axios({
    url: `${URL_API}/users`,
    method: 'GET',
  })
    .then(function (res) {
      disableLoading();
      renderUsersList(res.data);
      dsnd = res.data;
      console.log('🚀 ~ dsnd', dsnd);
    })
    .catch(function (err) {
      disableLoading();
      console.log(err);
    });
}
fetchUsersList();

function xoaNguoiDung(id) {
  enableLoading();
  axios({
    url: `${URL_API}/users/${id}`,
    method: 'DELETE',
  })
    .then(function (res) {
      location.reload();
      disableLoading();
      console.log(res);
    })
    .catch(function (err) {
      disableLoading();
      console.log(err);
    });
}

function themNguoiDung() {
  var user = layThongTinTuForm();

  // Tài khoản ND
  var isValidTK = true;
  isValidTK =
    kiemTraRong(user.taiKhoan, 'tbTaiKhoan', 'Tài khoản ND không để trống') &&
    kiemTraTrung(user.tkNV, dsnd);

  // Họ tên ND
  var isValidHT = true;
  isValidHT =
    kiemTraRong(user.hoTen, 'tbHoTen', 'Họ tên ND không để trống') &&
    kiemTraChu(user.hoTen, 'tbHoTen', 'Họ tên ND phải là không chứa ký tự đặt biệt');

  // Pass ND
  var isValidPass = true;
  isValidPass =
    kiemTraRong(user.matKhau, 'tbMatKhau', 'Mật khẩu ND không để trống') &&
    kiemTraMatKhau(user.matKhau);

  // Email
  var isValidEmail = true;
  isValidEmail =
    kiemTraRong(user.email, 'tbEmail', 'Email ND không để trống') && kiemTraEmail(user.email);

  // Hình ảnh
  var isValidImage = true;
  isValidImage = kiemTraRong(user.hinhAnh, 'tbHinhAnh', 'Hình ảnh không để trống');

  // Loại người dùng
  var isValidLN = true;
  isValidLN = kiemTraLuaChon('loaiNguoiDung', 'tbLoaiNguoiDung', 'Chưa chọn loại người dùng');

  // Loại ngôn ngữ
  var isValidNN = true;
  isValidNN = kiemTraLuaChon('loaiNgonNgu', 'tbLoaiNgonNgu', 'Chưa chọn loại ngôn ngữ');

  // Mô tả
  var isValidMT = true;
  isValidMT =
    kiemTraRong(user.moTa, 'tbMoTa', 'Mô tả không để trống') &&
    kiemTraMoTa(60, user.moTa, 'tbMoTa');

  var isValid =
    isValidTK &
    isValidHT &
    isValidPass &
    isValidEmail &
    isValidImage &
    isValidLN &
    isValidNN &
    isValidMT;

  if (isValid) {
    axios({
      url: `${URL_API}/users`,
      method: 'POST',
      data: user,
    })
      .then(function (res) {
        disableLoading();
        console.log(res);
        fetchUsersList();
        // getID('formND').reset();
      })
      .catch(function (err) {
        disableLoading();
        console.log(err);
      });
  }
}

function suaNguoiDung(id) {
  enableLoading();
  getID('btnCapNhatND').style.display = 'block';
  getID('btnThemND').style.display = 'none';
  axios({
    url: `${URL_API}/users/${id}`,
    method: 'GET',
  })
    .then(function (res) {
      disableLoading();
      console.log(res);
      hienThiThongTinLenForm(res.data);
      document.querySelector('#idND span').innerHTML = res.data.id;
    })
    .catch(function (err) {
      disableLoading();
      console.log(err);
    });
}

function capNhatNguoiDung() {
  var user = layThongTinTuForm();
  var idND = document.querySelector('#idND span').innerHTML * 1;
  enableLoading();
  axios({
    url: `${URL_API}/users/${idND}`,
    method: 'PUT',
    data: user,
  })
    .then(function (res) {
      disableLoading();
      console.log(res);
      fetchUsersList();
      getID('btnThemND').style.display = 'block';
    })
    .catch(function (err) {
      disableLoading();
      console.log(err);
    });
}

getID('btnThemNguoiDung').addEventListener('click', function () {
  getID('formND').reset();
  getID('idND').style.display = 'none';
  getID('btnCapNhatND').style.display = 'none';
});
